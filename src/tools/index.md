# Tools

## Introduction

The following tutorials will mainly focus on open-source software for numerous reasons. The software we are going to be using is free, faster, smaller and, in terms of creating content for Re-Volt, easier to use if you’re just starting.

## Blender - 3D Modeling
1. Install Blender from [**here**](https://download.blender.org/release/Blender2.79/latest/). Be sure to get the **2.79** series.
2. Download the Re-Volt add-on from [**here**](https://gitlab.com/re-volt/re-volt-addon/-/releases). Get the latest release.

## GIMP - Texture Creation
We will use GIMP for creating textures for the track. If you tried to use GIMP before and didn’t like it, don’t worry. You will see that it suits our needs perfectly. Everything you could want to do with it will be explained in a later step.

[Download GIMP](http://gimp.org)

## Audacity

This is an option. If you are planning to have some sounds in your track or a custom engine sound for your car, you might want to download this audio editor to get your sound files right.

[Download Audacity](http://audacityteam.org/)

## Re-Volt

The tutorials focuses on creating tracks for [RVGL](https://rvgl.re-volt.io). Get the game [here](http://re-volt.io/downloads/game).


That's it for now. You are pretty much covered. What you should have now:

+ Blender and the add-on
+ GIMP
+ Audacity (if you want to edit sounds)
+ A clean installation of Re-Volt