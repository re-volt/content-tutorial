# Blender: Setup

To install the add-on, follow the guide [here](https://re-volt.gitlab.io/re-volt-addon/installation.html) and come back when you're done.

When you have done that, close the preferences window. Then, with the cursor over the main viewport (with the cube inside of it), press A to select all objects. 

The selected objects will be outlined. Press Delete or X to delete these objects (since we don't need them right now). 

You can now resize the left sidebar of the 3D view. It contains the main RV tool panel at the very top.

When all that is done, press **CTRL U** to save the startup file (so you don't have to activate the plugin every time you start Blender).

Now Blender is set up for creating content for Re-Volt.