# Pos Nodes

## What are POS Nodes?

POS Nodes are used to determine your track’s length. The Re-Volt engine will render the Pos node line to use to calculate the distance between cars, thanks to POS nodes the information about other players will get supplied.

## Placing POS Nodes (Legacy control)

1. Check your .inf file for the STARTPOS parameter. This parameter will determine the first/last POSNODE. Check this [video](https://www.youtube.com/watch?v=rcpba7jzPKY) how to determine this first POSNODE.

2. Type in your game MAKEITGOOD as name, or use the DEV mode in v1.2 and rvgl, more information about this in the AI Nodes tutorial.

3. Go in your track that you want to make POS Nodes. Press F4 then press **Insert** to insert the first POS Node.

4. Now after you inserted the first POS Node , using the same ‘trick’ insert the second one.

> Better use a lot of POS Nodes because sometimes it’s more useful and revolt will give more and better infos. Each POS Node should be very near to the next.

5. OK , you did the first two POS Nodes. Now to do the next step just put the mouse over the **first** POS Nodes and press Spacebar. Then go with the mouse over the next POS Node and press Spacebar again. You will see a gradient line. This gradient will show you the direction.

6. Continue adding nodes until you covered your whole track. 

7. The last step is about connecting the nodes. Hover your mouse over the a node and press **Enter**. Now hover your mouse over the next node and press **Space**. Hover your mouse over the third node and press Space again. Keep going till you connected all nodes.

> After you have done the POS Nodes press left Ctrl + F4 to save. You will see a small window, you saved the POS Nodes in the \*.pan file that you should find in the track’s folder.

Tutorial by miromiro from Re-Volt Live, posted with permission (edited by javildesign)

## Modern Editor

If you like to use the new editor you can enable it by editing `profiles\rvgl.ini` in your RVGL folder.
Open this with your favorite text editor and find these lines: 

```
[Editor]
LegacyCameraControls = 0 // New editor controls enabled
LegacyEditorControls = 0 // New editor enabled
```

0 is on (new editor), 1 is off (legacy). Restart RVGL completely when you make a change.

Controls for POSNODES using the new editor are listed in the [RVGL documentation](https://re-volt.gitlab.io/rvgl-docs/advanced.html#pos-nodes).

## Tutorial POSNODES and STARTPOS

[![](http://img.youtube.com/vi/rcpba7jzPKY/0.jpg)](http://www.youtube.com/watch?v=rcpba7jzPKY "POSNODES")

