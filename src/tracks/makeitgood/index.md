Now it's time to integrate your track into the game. To do so, you will have to use the game's in-game editor.  
This is explained in detail in the next section:  

[MakeItGood](/03.makeitgood/)