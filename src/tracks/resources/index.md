# Resources

To create a track, you will need some resources.  
Textures, sounds and even models if you don't want to model everything yourself.

## Everything

[http://opengameart.org/](http://opengameart.org/) – Speaks for itself.

## Textures

[**RVIO Texture Animations**](http://files.re-volt.io/resources/animations.7z)  
More than 20 texture animations ready to be used with the Blender add-on.

[**RVIO Skyboxes**](http://files.re-volt.io/resources/skyboxes.7z)  
More than 20 skyboxes ready to be used in RVGL.

[http://cgtextures.com/](http://cgtextures.com/) – Huge resource.

## Sounds

[http://freesound.org/](http://freesound.org/) – Free sounds. Find login data at [http://bugmenot.com](http://bugmenot.com)

## Music

> TODO: Find a resource

## Useful Topics

- [**Alpha sorting**: How translucent polygons are rendered](https://forum.re-volt.io/viewtopic.php?f=9&t=223)