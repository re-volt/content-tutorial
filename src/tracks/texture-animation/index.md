# Creating Texture Animations in Blender

<!-- MarkdownTOC autolink='true' -->

- [Introduction](#introduction)
- [The Texture Animation Panel](#the-texture-animation-panel)
- [Setting up a Texture Animation](#setting-up-a-texture-animation)
	- [Linear Animation](#linear-animation)
	- [Grid Animation](#grid-animation)
- [Applying Animation to a Face](#applying-animation-to-a-face)
- [Texture Design Considerations](#texture-design-considerations)

<!-- /MarkdownTOC -->


## Introduction

Objects in a world file can have their textures animated. This guide takes you
through the process of creating texture animations and applying them to faces in
your world.

You can have any number of texture animations per world file, but the addon is currently
limited to 64 slots. Each animation is similar to a *gif* file or video, which consists of 
a number of frames. Each frame must be mapped to a region within one of the track textures
(eg, *tracka.bmp*).

Give the [Texture Animation reference manual](https://re-volt.gitlab.io/re-volt-addon/tools-panel/texture-animation.html)
a quick glance and then read on.

![Animation Preview](texanim-preview.gif)

## The Texture Animation Panel

Texture animations are set up as part of the scene. They are independent of any object or
face in your world file. However, you still access them through Blender's Edit Mode because
it can be convenient in some cases. You must be careful about this, because selecting a
face does *not* update the face's texture animation slot in the panel!

Select an object in your world file, then hit TAB to enter the Edit Mode. In the Re-Volt
panel, you now have access to a new section called **Texture Animation (.w)**. This is where
you set up your animations.

![Texture Animation Panel](texanim.png)

## Setting up a Texture Animation

Set **Total Slots** to the total number of animations you plan on having in your track. Select
an animation slot and set the total number of frames for this animation. Under **Animation Frame**,
select Frame 0 and adjust its properties:

- **Texture**: Texture page number for this frame (0 is *tracka.bmp*, and so on).
- **Duration**: Number of seconds to display this frame.
- **UV Coordinates**: 3 or 4 UV coordinates to define the face mapping for this frame. If you
  have a face selected, use the **UV to Frame** button to copy the face's mapping to the 
  frame data. Note that these frame coordinates are shared by all faces that are using this
  animation slot.

> There is a Preview feature, but this is currently broken, because only the UV coordinates are
updated. The face is still rendered with its mapped texture, but this is really the texture animation
slot and not its actual texture. Read the [Applying Animation to a Face](#applying-animation-to-a-face)
section for a better explanation.

You can select further frames and fill in their properties by hand. However, this will not be
required for majority of use cases, because the addon can automatically generate two of the
most prominent type of animations - Linear and Grid.

### Linear Animation

Each frame moves through the texture page from point A to point B (up-down or left-to-right).
This gives you a scrolling effect. The Museum 2 elevator should be a familiar example.

Ensure that the first and last frame properties are properly filled in. Then click the **Animate...**
drop down and select **Transform Animation**.

![Texture Animation Panel: Transform](texanim-transform.png)

Fill in the values - set the Start and End frames, the duration of each frame and the texture
page to be used, then press OK. This will interpolate between the Start and End frames and fill
in the intermediate frame UV coordinates.

### Grid Animation

The texture page is treated as an M x N grid. Each frame maps to a successive element / region
from this grid. The Mars animation in Museum 2 is an example of this type.

Fill in the first frame properties and set the UV coordinates to the top left corner of the
grid texture. From the **Animate...** drop down, select **Grid Animation**.

![Texture Animation Panel: Grid](texanim-grid.png)

Set the Start frame (typically 0), the duration of each frame and the texture page to be
used. Set the X and Y resolution to the number of elements across and down the grid,
respectively. As an example, 4 x 4 would give you an animation with 16 frames.

## Applying Animation to a Face

This is probably the most confusing part, because faces handle their texture animation
information in a rather odd way. This goes way back to some design decisions by Re-Volt's
original developers.

> A face with Texture Animation enabled does not use its own texture page or UV coordinates.
It sources both these information from the animation's current frame. This unused texture
mapping information is really used to specify the face's animation slot!

Follow the [Texture Mapping](../texture-mapping/index.html) guide if you're unsure about
how to assign textures to faces. Also learn how to assign [Polygon Properties](../polygon-properties/index.html).

To apply Animation #2 to some faces, select these faces and enable the **Texture Animation**
property checkbox from the **Face Properties** section.

![Texture Animation Property](texanim-prop.png)

Switch to the **UV/Image Editor** and assign *2.bmp* to these faces (*trackc.bmp*).
Note, again, that the faces are not really going to be mapped to *trackc.bmp*. They'll be
mapped to whatever texture page number was specified in the Frame data for Animation #2.

![Texture Assignment](texture-assign.png)

## Texture Design Considerations

When applying animation to a mesh containing multiple faces, it should be noted that the entire
animation is repeated across each face. i.e., every face using a certain animation slot is mapped
to the exact same UV coordinates within the same texture page.

To give the impression that the faces are animating together, as a whole, care must be taken to
ensure that the texture itself tiles seamlessly on all sides. Look for some tutorials on how
to create seamless textures in the image editor of your choice.

![Seamless Texture](texture-tile.png)

[Tutorial: How To Create A Seamless Texture in GIMP](https://www.gameartguppy.com/tutorial-how-to-create-a-seamless-texture-in-gimp/)

