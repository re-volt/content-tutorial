# Tutorials for creating Content for Re-Volt

[re-volt.gitlab.io/content-tutorial](https://re-volt.gitlab.io/content-tutorial)

This tutorial is about creating content for Re-Volt in Blender.

## Building the Docs Locally

Install mdbook and run `mdbook build` or `mdbook serve` in the project dir.

The docs are automatically built with CI after pushing to GitLab.
